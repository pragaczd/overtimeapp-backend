package pl.daniel.aplikacja.aplikacja.Entity;


import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "overtime_records")
public class OvertimeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "id_username")
    private Integer idUsername;

    @Column(name = "username")
    private String username;

    @Column(name = "date_add")
    private LocalDate dateAdd;

    @Column(name = "balance")
    private Integer balance;

    @Column(name = "recently_added_overtime")
    private Integer recentlyAddedOvertime;

    @Column(name = "overtime_date")
    private LocalDate overtimeDate;

    @Column(name = "full_overtime_date")
    private LocalDateTime fullOvertimeDate;

    @Column(name = "is_registration_record")
    private boolean isRegistrationRecord;

    @PrePersist
    void preInsert() {
        setDateAdd(LocalDate.now());
        setFullOvertimeDate(LocalDateTime.now());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUsername() {
        return idUsername;
    }

    public void setIdUsername(Integer idUsername) {
        this.idUsername = idUsername;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public LocalDate getDateAdd() {
        return dateAdd;
    }

    public void setDateAdd(LocalDate dateAdd) {
        this.dateAdd = dateAdd;
    }

    public Integer getBalance() {
        if(balance == null){
            return 0;
        }
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getRecentlyAddedOvertime() {
        return recentlyAddedOvertime;
    }

    public void setRecentlyAddedOvertime(Integer recentlyAddedOvertime) {
        this.recentlyAddedOvertime = recentlyAddedOvertime;
    }

    public LocalDate getOvertimeDate() {
        return overtimeDate;
    }

    public void setOvertimeDate(LocalDate overtimeDate) {
        this.overtimeDate = overtimeDate;
    }

    public LocalDateTime getFullOvertimeDate() {
        return fullOvertimeDate;
    }

    public void setFullOvertimeDate(LocalDateTime fullOvertimeDate) {
        this.fullOvertimeDate = fullOvertimeDate;
    }

    public boolean isIsRegistrationRecord() {
        return isRegistrationRecord;
    }

    public void setIsRegistrationRecord(boolean display) {
        this.isRegistrationRecord = display;
    }
}
