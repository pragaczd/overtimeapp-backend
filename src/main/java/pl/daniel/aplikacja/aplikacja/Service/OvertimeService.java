package pl.daniel.aplikacja.aplikacja.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.daniel.aplikacja.aplikacja.Calculations.BalanceCalculation;
import pl.daniel.aplikacja.aplikacja.Entity.OvertimeEntity;
import pl.daniel.aplikacja.aplikacja.Entity.UserEntity;
import pl.daniel.aplikacja.aplikacja.Repository.OvertimeRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OvertimeService extends BalanceCalculation {

    @Autowired
    OvertimeRepository overtimeRepository;

    public List<OvertimeEntity> findAllOvertimeRecordsForUser(Integer idUsername) {
        return overtimeRepository.findAllByIdUsernameAndIsRegistrationRecordIsTrue(idUsername);
    }

    public List<OvertimeEntity> findAllOvertimeRecordsForUserByUsername(String username) {
        return overtimeRepository.findAllByUsernameAndIsRegistrationRecordIsTrue(username);
    }

    public OvertimeEntity addOvertimeForUser(OvertimeEntity overtimeEntity) {
        overtimeEntity.setBalance(balanceCalculation(overtimeEntity, overtimeRepository.findTopByIdUsernameOrderByFullOvertimeDateDesc(overtimeEntity.getIdUsername())));
        overtimeEntity.setIdUsername(overtimeEntity.getIdUsername());
        overtimeEntity.setUsername(overtimeEntity.getUsername());
        overtimeEntity.setIsRegistrationRecord(true);
        return overtimeRepository.save(overtimeEntity);
    }

    public OvertimeEntity addOvertimeForNewUser(UserEntity userEntity) {
        OvertimeEntity overtimeEntity = new OvertimeEntity();
        overtimeEntity.setIdUsername(userEntity.getId());
        overtimeEntity.setUsername(userEntity.getUsername());
        overtimeEntity.setBalance(0);
        overtimeEntity.setRecentlyAddedOvertime(0);
        overtimeEntity.setOvertimeDate(LocalDate.now());
        overtimeEntity.setIsRegistrationRecord(false);
        return overtimeRepository.save(overtimeEntity);
    }

    public List<OvertimeEntity> getAllOvertimesForAllUsers(){
        List<OvertimeEntity> list = new ArrayList<>();
       overtimeRepository.findAllByIsRegistrationRecordIsTrue().iterator().forEachRemaining(list::add);
       return list;
    }
}
