package pl.daniel.aplikacja.aplikacja.Service;

import org.springframework.stereotype.Service;
import pl.daniel.aplikacja.aplikacja.Entity.UserEntity;
import pl.daniel.aplikacja.aplikacja.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserEntity> findAll(){
        List<UserEntity> list = new ArrayList<>();
        userRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    public List<UserEntity> findAllActiveUsers(){
        List<UserEntity> list = new ArrayList<>();
        userRepository.findAllByActiveIsTrue().iterator().forEachRemaining(list::add);
        return list;
    }

    public UserEntity findById(Integer id){
        Optional<UserEntity> optionalUser = userRepository.findById(id);
        return optionalUser.isPresent() ? optionalUser.get() : null;
    }

    public Optional<UserEntity> findByUsername(String username){
        Optional<UserEntity> listByUsername = userRepository.findByUsername(username);
        return listByUsername;
    }

    public UserEntity findOneByUsername(String username){
        Optional<UserEntity> optionalUser = Optional.ofNullable(userRepository.findOneByUsername(username));
        return optionalUser.isPresent() ? optionalUser.get() : null;
    }

    public UserEntity save(UserEntity userEntity){
        return userRepository.save(userEntity);
    }

    public void deleteById(Integer id) {
        userRepository.deleteById(id);
    }

    public void deleteByUsername(String userName){ userRepository.deleteByUsername(userName);}

}
