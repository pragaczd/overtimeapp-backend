package pl.daniel.aplikacja.aplikacja.Repository;

import org.springframework.data.repository.CrudRepository;
import pl.daniel.aplikacja.aplikacja.Entity.UserEntity;

import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {

    Optional<UserEntity> findByUsername(String username);
    UserEntity findOneByUsername(String username);
    String deleteByUsername(String username);
    Iterable<UserEntity> findAllByActiveIsTrue();
}
