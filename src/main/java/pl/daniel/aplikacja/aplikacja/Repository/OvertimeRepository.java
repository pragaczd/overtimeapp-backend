package pl.daniel.aplikacja.aplikacja.Repository;

import org.springframework.data.repository.CrudRepository;
import pl.daniel.aplikacja.aplikacja.Entity.OvertimeEntity;

import java.util.List;

public interface OvertimeRepository extends CrudRepository<OvertimeEntity, Integer>{

    List<OvertimeEntity> findAllByIdUsernameAndIsRegistrationRecordIsTrue(Integer id_username);
    List<OvertimeEntity> findAllByUsernameAndIsRegistrationRecordIsTrue(String username);
    OvertimeEntity findTopByIdUsernameOrderByFullOvertimeDateDesc(Integer id_username);
    List<OvertimeEntity> findAllByIsRegistrationRecordIsTrue();
}
