package pl.daniel.aplikacja.aplikacja.Calculations;

import pl.daniel.aplikacja.aplikacja.Entity.OvertimeEntity;

public class BalanceCalculation {

    public Integer balanceCalculation(OvertimeEntity overtimeEntity, OvertimeEntity overtimeRecord) {
        Integer recentlyAddOvertime = overtimeEntity.getRecentlyAddedOvertime();

        return overtimeRecord.getBalance() + recentlyAddOvertime;

    }

}
