package pl.daniel.aplikacja.aplikacja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import pl.daniel.aplikacja.aplikacja.Repository.FilesStorageService;

import javax.annotation.Resource;


@SpringBootApplication
@EnableJpaRepositories(basePackages = {"pl.daniel.aplikacja.aplikacja.Repository"})
public class AplikacjaApplication {

    @Resource
    FilesStorageService storageService;

    public static void main(String[] args) {
        SpringApplication.run(AplikacjaApplication.class, args);
    }

}