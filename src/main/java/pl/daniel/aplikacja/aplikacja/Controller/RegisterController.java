package pl.daniel.aplikacja.aplikacja.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.daniel.aplikacja.aplikacja.Entity.UserEntity;
import pl.daniel.aplikacja.aplikacja.Service.OvertimeService;
import pl.daniel.aplikacja.aplikacja.Service.UserService;

@RestController
@RequestMapping("/register")
@CrossOrigin(value = "http://localhost:4200", maxAge = 3600)
public class RegisterController {

    @Autowired
    private UserService userService;

    @Autowired
    private OvertimeService overtimeService;

    @PostMapping
    public UserEntity saveUser(@RequestBody UserEntity user){
        userService.save(user);
        overtimeService.addOvertimeForNewUser(user);
        return user;
    }
}
