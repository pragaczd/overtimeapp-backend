package pl.daniel.aplikacja.aplikacja.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.daniel.aplikacja.aplikacja.Entity.UserEntity;
import pl.daniel.aplikacja.aplikacja.Service.UserService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
@CrossOrigin(value = "http://localhost:4200", maxAge = 3600)
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    public UserEntity getOne(@PathVariable Integer id){
        return userService.findById(id);
    }

    @GetMapping("/getUserByUsername/{username}")
    public Optional<UserEntity> getOneByUsername(@PathVariable String username) {
        return userService.findByUsername(username);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserEntity> listUser(){
        return userService.findAll();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){
        userService.deleteById(id);
    }

    @DeleteMapping("/deleteUser/{username}")
    public void deleteByUsername(@PathVariable String username){ userService.deleteByUsername(username);}

    @PostMapping("/saveEditedUser")
    public UserEntity saveEditedUser(@RequestBody UserEntity userEntity){
        Optional<UserEntity> user = userService.findByUsername(userEntity.getUsername());
        user.get().setDescription(userEntity.getDescription());
        return userService.save(user.get());
    }
}
