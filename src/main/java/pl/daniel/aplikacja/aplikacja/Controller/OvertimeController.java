package pl.daniel.aplikacja.aplikacja.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.daniel.aplikacja.aplikacja.Entity.OvertimeEntity;
import pl.daniel.aplikacja.aplikacja.Entity.UserEntity;
import pl.daniel.aplikacja.aplikacja.Service.OvertimeService;
import pl.daniel.aplikacja.aplikacja.Service.UserService;

import java.util.*;

@RestController
@RequestMapping("/overtime")
@CrossOrigin(value = "http://localhost:4200", maxAge = 3600)
public class OvertimeController {

    @Autowired
    private OvertimeService overtimeService;

    @Autowired
    private UserService userService;

    @GetMapping("/allRecords/{userId}")
    public List<OvertimeEntity> getAllByUserId(@PathVariable Integer userId) {
        return overtimeService.findAllOvertimeRecordsForUser(userId);
    }

    @PostMapping("/addOvertimeForUser")
    public OvertimeEntity addOvertimeForUser(@RequestBody OvertimeEntity overtimeEntity) {
        return overtimeService.addOvertimeForUser(overtimeEntity);
    }

    @GetMapping("/allRecords")
    public List<OvertimeEntity> getAllOvertimesForAllUsers() {
        List<OvertimeEntity> overtimes = new ArrayList<>();
        List<UserEntity> userEntityList = userService.findAllActiveUsers();
        for (UserEntity userEntity : userEntityList) {
            overtimeService.findAllOvertimeRecordsForUser(userEntity.getId()).iterator().forEachRemaining(overtimes::add);
        }
        return overtimes;

    }

    @GetMapping("/allRecordsByUsername/{username}")
    public List<OvertimeEntity> getAllByUsername(@PathVariable String username) {
        return overtimeService.findAllOvertimeRecordsForUserByUsername(username);
    }

}
